import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  title = new BehaviorSubject('');
  icon = new BehaviorSubject('');
  iconClickHandler = new BehaviorSubject(() => {});
  refreshHandler = new BehaviorSubject(() => {});

  constructor() { }

  setTitle(title: string) {
    this.title.next(title);
  }
  setIcon(icon: string) {
    this.icon.next(icon);
  }
  setIconClickHandler(handler: () => void) {
    this.iconClickHandler.next(handler);
  }
  setRefreshHandler(handler: () => void) {
    this.refreshHandler.next(handler);
  }
}
