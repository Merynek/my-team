import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GameStatisticsComponent } from './game-statistics.component';
import {TranslateModule} from '@ngx-translate/core';
import {Game} from '../../../data/interface/game';

describe('GameStatisticsComponent', () => {
  let component: GameStatisticsComponent;
  let fixture: ComponentFixture<GameStatisticsComponent>;
  const prepareData = () => {
    component.game = {
      homeTeam: {
        penalty: 1,
        ppGoals: 2,
        shGoals: 3,
        shotsOnGoal: 4
      },
      visitorTeam: {
        penalty: 1,
        ppGoals: 2,
        shGoals: 3,
        shotsOnGoal: 4
      }
    } as Game;
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot()
      ],
      declarations: [ GameStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameStatisticsComponent);
    component = fixture.componentInstance;
    prepareData();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
