import {Component, Input, OnInit} from '@angular/core';
import {Game, GameState} from '../../../data/interface/game';
import {Router} from '@angular/router';

@Component({
  selector: 'app-game-group',
  templateUrl: './game-group.component.html',
  styleUrls: ['./game-group.component.less']
})
export class GameGroupComponent implements OnInit {
  @Input() games: Game[];
  @Input() gameState: GameState;
  filteredGames: Game[];

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.filteredGames = this.games.filter(game => {
      return game.type === this.gameState;
    });
  }

  canShowTitle(): boolean {
    return this.filteredGames.length > 0 && !(this.gameState === GameState.LIVE);
  }

  canShowDetail(game: Game) {
    return game.type === GameState.FINISHED;
  }
  showDetail(game: Game) {
    if (this.canShowDetail(game)) {
      this.router.navigate(['/detail/', game.id]);
    }
  }
}
