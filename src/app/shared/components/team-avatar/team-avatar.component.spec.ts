import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TeamAvatarComponent } from './team-avatar.component';
import {Team} from '../../../data/interface/game';

describe('TeamAvatarComponent', () => {
  let component: TeamAvatarComponent;
  let fixture: ComponentFixture<TeamAvatarComponent>;
  const prepareData = () => {
    component.team = {
      shortcut: 'TEST',
      logo: {
        urlS: ''
      }
    } as Team;
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamAvatarComponent);
    component = fixture.componentInstance;
    prepareData();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render shortcut of team', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('span').textContent).toContain('TEST');
  });
});
