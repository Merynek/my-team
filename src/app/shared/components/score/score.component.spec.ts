import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ScoreComponent } from './score.component';

describe('ScoreComponent', () => {
  let component: ScoreComponent;
  let fixture: ComponentFixture<ScoreComponent>;
  const prepareData = (score: string) => {
    component.score = score;
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoreComponent);
    component = fixture.componentInstance;
    prepareData('5:2pen');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('correct set score without another info', () => {
    expect(component.homeScore).toBe('5');
    expect(component.visitorScore).toBe('2');
  });
});
