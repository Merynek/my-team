import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {Game, GameState} from '../../../data/interface/game';

@Component({
  selector: 'app-game-row',
  templateUrl: './game-row.component.html',
  styleUrls: ['./game-row.component.less']
})

export class GameRowComponent implements OnInit {
  @Input() game: Game;
  @HostBinding('class') class;
  dayKey: string;
  date: string;
  gameState = GameState;

  constructor() { }

  ngOnInit(): void {
    this.prepareDateText();
    this.class = this.game.type;
  }

  private prepareDateText() {
    const re = /\((.*)\)/i;
    const dayData = this.game.dateStartText.split(' ');

    this.dayKey = dayData[1].match(re)[1];
    this.date = dayData[0];
  }
}
