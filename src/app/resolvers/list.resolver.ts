import { Injectable } from '@angular/core';
import { DataService } from '../data/service/data.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import {Game} from '../data/interface/game';

@Injectable()
export class ListResolver implements Resolve<Game[]> {
  constructor(public dataService: DataService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Game[]>|Promise<Game[]>|Game[] {
    return this.dataService.getGames();
  }
}
