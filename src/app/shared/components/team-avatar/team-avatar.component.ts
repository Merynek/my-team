import {Component, Input, OnInit} from '@angular/core';
import {Team} from '../../../data/interface/game';

@Component({
  selector: 'app-team-avatar',
  templateUrl: './team-avatar.component.html',
  styleUrls: ['./team-avatar.component.less']
})
export class TeamAvatarComponent implements OnInit {
  @Input() team: Team;

  constructor() { }

  ngOnInit(): void {
  }
}
