import { Pipe, PipeTransform } from '@angular/core';
import {Game} from '../../data/interface/game';

@Pipe({
  name: 'gamePipe'
})
export class GamePipe implements PipeTransform {

  transform(games: Game[]): Game[] {
    return games.sort((a, b) => {
      return a.id - b.id;
    }).reverse();
  }
}
