import {Component, HostBinding, OnDestroy, OnInit} from '@angular/core';
import {filter, map, take, timeout} from 'rxjs/operators';
import {Game} from '../../data/interface/game';
import {ActivatedRoute, Router} from '@angular/router';
import {HeaderService} from '../../shared/services/header/header-service';
import {DataService} from '../../data/service/data.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.less']
})
export class DetailComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  @HostBinding('class') class;
  game: Game;

  constructor(
    private route: ActivatedRoute,
    private headerService: HeaderService,
    private dataService: DataService,
    private router: Router
  ) { }

  private static getTitle(game: Game) {
    return `${game.homeTeam.shortcut} vs ${game.visitorTeam.shortcut}`;
  }

  private initData(game: Game) {
    this.game = game;
    this.class = game.type;
    this.headerService.setTitle(DetailComponent.getTitle(game));
  }

  ngOnInit(): void {
    this.route.data.pipe(map(data => data.game)).subscribe((game: Game) => {
      this.initData(game);
    });
    this.headerService.setIcon('close');
    this.headerService.setIconClickHandler(() => {
      this.router.navigate(['/list']);
    });
    this.headerService.setRefreshHandler(() => {
      this.refresh();
    });
    this.subscription = this.dataService.timeout
      .pipe(filter(data => data))
      .subscribe(() => {
        this.refresh();
      });
  }

  refresh() {
    this.dataService.getGame(this.game.id.toString()).subscribe((game: Game) => {
      this.initData(game);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
