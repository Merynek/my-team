import {Component, Input, OnInit} from '@angular/core';
import {Game} from '../../../data/interface/game';

@Component({
  selector: 'app-game-face-offs',
  templateUrl: './game-face-offs.component.html',
  styleUrls: ['./game-face-offs.component.less']
})
export class GameFaceOffsComponent implements OnInit {
  @Input() game: Game;
  faceOffsHome: number;
  faceOffsVisitor: number;

  constructor() { }

  ngOnInit(): void {
    this.calculateFaceOffs();
  }

  private calculateFaceOffs() {
    const homeTeamData = this.game.homeTeam.faceoffs.split('/');
    const visitorTeamData = this.game.visitorTeam.faceoffs.split('/');
    const total = Number(homeTeamData[1]);
    const calculate = (value) => {
      return total ? Math.round((100 * value) / total) : 0;
    };

    this.faceOffsHome = calculate(homeTeamData[0]);
    this.faceOffsVisitor = calculate(visitorTeamData[0]);
  }
}
