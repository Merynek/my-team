import {Component, Input, OnInit} from '@angular/core';
import {Game} from '../../../data/interface/game';

@Component({
  selector: 'app-game-statistics',
  templateUrl: './game-statistics.component.html',
  styleUrls: ['./game-statistics.component.less']
})
export class GameStatisticsComponent implements OnInit {
  @Input() game: Game;

  constructor() { }

  ngOnInit(): void {
  }
}
