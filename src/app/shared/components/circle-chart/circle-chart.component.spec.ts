import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CircleChartComponent } from './circle-chart.component';

describe('CircleChartComponent', () => {
  let component: CircleChartComponent;
  let fixture: ComponentFixture<CircleChartComponent>;
  const prepareData = () => {
    component.percent = 50;
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CircleChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CircleChartComponent);
    component = fixture.componentInstance;
    prepareData();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render percentage', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.percentage').textContent).toContain('50');
  });
});
