import {Component, Input, OnInit} from '@angular/core';
import {Game} from '../../../data/interface/game';

@Component({
  selector: 'app-game-attendance',
  templateUrl: './game-attendance.component.html',
  styleUrls: ['./game-attendance.component.less']
})
export class GameAttendanceComponent implements OnInit {
  @Input() game: Game;
  attendancePercentage: number;

  constructor() { }

  ngOnInit(): void {
    this.calculateAttendance();
  }

  private calculateAttendance() {
    const total = this.game.maxAttendance;

    this.attendancePercentage = Math.round((100 * this.game.attendance) / total);
  }

}
