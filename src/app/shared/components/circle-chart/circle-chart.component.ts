import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-circle-chart',
  templateUrl: './circle-chart.component.html',
  styleUrls: ['./circle-chart.component.less']
})
export class CircleChartComponent implements OnInit {
  @Input() percent: number;

  constructor() { }
  strokeDasharray;

  ngOnInit(): void {
    this.strokeDasharray = this.getStrokeDasharray();
  }

  private getStrokeDasharray() {
    return `${this.percent}, 100`;
  }

}
