import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GameGroupComponent } from './game-group.component';
import {Game} from '../../../data/interface/game';
import { GamePipe } from '../../pipes/game-pipe';
import { RouterTestingModule } from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';

describe('GameGroupComponent', () => {
  let component: GameGroupComponent;
  let fixture: ComponentFixture<GameGroupComponent>;
  const prepareGame = () => {
    return {
      roundText: 'PR2',
      score: '3:2',
      dateStartText: '7.3. (ST)',
      homeTeam: {
        shortcut: '',
        logo: {
          urlS: ''
        }
      },
      visitorTeam: {
        shortcut: '',
        logo: {
          urlS: ''
        }
      }
    } as Game;
  };
  const prepareData = () => {
    component.games = [
      prepareGame(),
      prepareGame()
    ];
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, TranslateModule.forRoot()],
      declarations: [ GameGroupComponent, GamePipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameGroupComponent);
    component = fixture.componentInstance;
    prepareData();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
