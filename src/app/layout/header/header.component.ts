import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {HeaderService } from '../../shared/services/header/header-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  title: string;
  icon: string;
  iconClickHandler: () => void;
  refreshHandler: () => void;

  constructor(
    public translate: TranslateService,
    private headerService: HeaderService
  ) { }

  ngOnInit() {
    this.headerService.title.subscribe(updatedTitle => {
      this.title = updatedTitle;
    });
    this.headerService.icon.subscribe(updatedIcon => {
      this.icon = updatedIcon;
    });
    this.headerService.iconClickHandler.subscribe(updatedIconClickHandler => {
      this.iconClickHandler = updatedIconClickHandler;
    });
    this.headerService.refreshHandler.subscribe(updatedRefreshHandler => {
      this.refreshHandler = updatedRefreshHandler;
    });
  }

}
