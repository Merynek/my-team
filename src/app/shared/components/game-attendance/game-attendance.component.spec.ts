import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GameAttendanceComponent } from './game-attendance.component';
import {Game} from '../../../data/interface/game';
import {TranslateModule} from '@ngx-translate/core';

describe('GameAttendanceComponent', () => {
  let component: GameAttendanceComponent;
  let fixture: ComponentFixture<GameAttendanceComponent>;
  const prepareData = () => {
    component.game = {
      roundText: 'PR2',
      score: '3:2',
      attendance: 1000,
      maxAttendance: 10000,
      dateStartText: '7.3. (ST)',
      homeTeam: {
        faceoffs: '41/65',
        shortcut: '',
        logo: {
          urlS: ''
        }
      },
      visitorTeam: {
        shortcut: '',
        faceoffs: '24/65',
        logo: {
          urlS: ''
        }
      }
    } as Game;
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ GameAttendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameAttendanceComponent);
    component = fixture.componentInstance;
    prepareData();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('correct compute attendance percentage', () => {
    const compiled = fixture.nativeElement;
    expect(component.attendancePercentage).toBe(10);

    expect(compiled.querySelector('.attendance-count').textContent).toContain('1000');
  });
});
