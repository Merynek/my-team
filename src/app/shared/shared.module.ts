import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameRowComponent } from './components/game-row/game-row.component';
import { TeamAvatarComponent } from './components/team-avatar/team-avatar.component';
import { ScoreComponent } from './components/score/score.component';
import { GameGroupComponent } from './components/game-group/game-group.component';
import {TranslateModule} from '@ngx-translate/core';
import {RouterModule} from '@angular/router';
import { GamePipe } from './pipes/game-pipe';
import { GameFaceOffsComponent } from './components/game-face-offs/game-face-offs.component';
import { LineChartComponent } from './components/line-chart/line-chart.component';
import { GameAttendanceComponent } from './components/game-attendance/game-attendance.component';
import { GameStatisticsComponent } from './components/game-statistics/game-statistics.component';
import { CircleChartComponent } from './components/circle-chart/circle-chart.component';
import { ScreenLoaderComponent } from './components/screen-loader/screen-loader.component';

@NgModule({
  declarations: [
    GameRowComponent,
    TeamAvatarComponent,
    ScoreComponent,
    GameGroupComponent,
    GamePipe,
    GameFaceOffsComponent,
    LineChartComponent,
    GameAttendanceComponent,
    GameStatisticsComponent,
    CircleChartComponent,
    ScreenLoaderComponent
  ],
  exports: [
    GameRowComponent,
    GameGroupComponent,
    TeamAvatarComponent,
    ScoreComponent,
    GameFaceOffsComponent,
    LineChartComponent,
    GameAttendanceComponent,
    GameStatisticsComponent,
    ScreenLoaderComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule
  ]
})
export class SharedModule { }
