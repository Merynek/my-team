import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GameFaceOffsComponent } from './game-face-offs.component';
import {Game} from '../../../data/interface/game';
import {TranslateModule} from '@ngx-translate/core';

describe('GameFaceOffsComponent', () => {
  let component: GameFaceOffsComponent;
  let fixture: ComponentFixture<GameFaceOffsComponent>;
  const prepareData = () => {
    component.game = {
      roundText: 'PR2',
      score: '3:2',
      dateStartText: '7.3. (ST)',
      homeTeam: {
        faceoffs: '41/65',
        shortcut: '',
        logo: {
          urlS: ''
        }
      },
      visitorTeam: {
        shortcut: '',
        faceoffs: '24/65',
        logo: {
          urlS: ''
        }
      }
    } as Game;
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ GameFaceOffsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameFaceOffsComponent);
    component = fixture.componentInstance;
    prepareData();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('correct compute faceoffs percentage', () => {
    expect(component.faceOffsHome).toBe(63);
    expect(component.faceOffsVisitor).toBe(37);
  });
});
