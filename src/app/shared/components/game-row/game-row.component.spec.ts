import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GameRowComponent } from './game-row.component';
import {Game} from '../../../data/interface/game';
import {TeamAvatarComponent} from '../team-avatar/team-avatar.component';
import {TranslateModule} from '@ngx-translate/core';

describe('GameRowComponent', () => {
  let component: GameRowComponent;
  let fixture: ComponentFixture<GameRowComponent>;
  const prepareData = () => {
    component.game = {
      roundText: 'PR2',
      score: '3:2',
      dateStartText: '7.3. (ST)',
      homeTeam: {
        shortcut: '',
        logo: {
          urlS: ''
        }
      },
      visitorTeam: {
        shortcut: '',
        logo: {
          urlS: ''
        }
      }
    } as Game;
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot()
      ],
      declarations: [ GameRowComponent, TeamAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameRowComponent);
    component = fixture.componentInstance;
    prepareData();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('correct render date', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.day').textContent).toContain('days.ST 7.3');
  });
});
