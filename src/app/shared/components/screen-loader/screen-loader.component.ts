import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-screen-loader',
  templateUrl: './screen-loader.component.html',
  styleUrls: ['./screen-loader.component.less']
})
export class ScreenLoaderComponent implements OnInit {
  @Input() showOverlay;

  constructor() { }

  ngOnInit(): void {
  }
}
