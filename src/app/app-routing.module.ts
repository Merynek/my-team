import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListComponent} from './modules/list/list.component';
import {DetailComponent} from './modules/detail/detail.component';
import {GameResolver} from './resolvers/game.resolver';
import {ListResolver} from './resolvers/list.resolver';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full'},
  {
    path: 'list',
    component: ListComponent,
    resolve: {
      games: ListResolver
    }
  },
  {
    path: 'detail/:id',
    component: DetailComponent,
    resolve: {
      game: GameResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
