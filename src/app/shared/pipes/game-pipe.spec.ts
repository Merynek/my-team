import { GamePipe } from './game-pipe';
import {Game} from '../../data/interface/game';

function prepareData(): Game[] {
  return [
    { id: 18 },
    { id: 1 },
    { id: 22 },
    { id: 8 },
    { id: 6 },
  ] as Game[];
}

describe('GamePipePipe', () => {
  it('create an instance', () => {
    const pipe = new GamePipe();
    expect(pipe).toBeTruthy();
  });

  it('correct sort', () => {
    const pipe = new GamePipe();
    const expectedData = [
      { id: 22 },
      { id: 18 },
      { id: 8 },
      { id: 6 },
      { id: 1 }
    ];

    // @ts-ignore
    expect(pipe.transform(prepareData())).toEqual(expectedData);
  });
});
