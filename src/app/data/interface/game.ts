interface Logo {
  urlS: string;
}

export interface Team {
  id: number;
  shortcut: string;
  title: string;
  faceoffs: string;
  penalty: number;
  ppGoals: number;
  shGoals: number;
  shotsOnGoal: number;
  logo: Logo;
}

export enum GameState {
  LIVE = 'live',
  PLANNED  = 'planned',
  FINISHED = 'finished'
}

export interface Game {
  id: number;
  type: GameState;
  dateStart: string;
  dateStartText: string;
  roundText: string;
  attendance: number;
  maxAttendance: number;
  score: string;
  homeTeam: Team;
  visitorTeam: Team;
}
