import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {BehaviorSubject, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import { Game } from '../interface/game';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private REFRESH_TIME = 10000; // 10s
  private REST_API_GAMES = 'games';
  private REST_API_GAME_DETAIL = 'games/';
  timeout = new BehaviorSubject(false);

  constructor(private httpClient: HttpClient) {
    // can be connect to webSocket instead of this ugly interval :-)
    setInterval(() => {
      this.timeout.next(true);
    }, this.REFRESH_TIME);
  }

  static handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getGames() {
    return this.httpClient.get<Game[]>(this.REST_API_GAMES).pipe(retry(3), catchError(DataService.handleError));
  }

  public getGame(id: string) {
    return this.httpClient.get<Game>(this.REST_API_GAME_DETAIL + id).pipe(retry(3), catchError(DataService.handleError));
  }
}
