import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.less']
})
export class ScoreComponent implements OnInit {
  @Input() score: string;
  homeScore: string;
  visitorScore: string;

  constructor() { }

  ngOnInit(): void {
    this.prepareScore();
  }

  private prepareScore() {
    const scoreData = this.score.split(':');
    const visitorScore = scoreData[1].replace(/([a-z]+)/i, ' $1 ').split(/[^0-9a-z]+/ig);

    this.homeScore = scoreData[0];
    this.visitorScore = visitorScore[0];
  }
}
