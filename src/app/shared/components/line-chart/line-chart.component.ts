import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.less']
})
export class LineChartComponent implements OnInit {
  @Input() percent: number;
  strokeDasharray;

  constructor() { }

  ngOnInit(): void {
    this.strokeDasharray = this.getStrokeDasharray();
  }

  private getStrokeDasharray() {
    return `${this.percent}, 100`;
  }
}
