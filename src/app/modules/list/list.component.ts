import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Game, GameState} from '../../data/interface/game';
import {filter, map } from 'rxjs/operators';
import {HeaderService} from '../../shared/services/header/header-service';
import {DataService} from '../../data/service/data.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  games: Game[] = [];
  gameState = GameState;

  constructor(
    private route: ActivatedRoute,
    private headerService: HeaderService,
    private dataService: DataService) { }

  private initData(games: Game[]) {
    games[2].type = GameState.PLANNED;
    this.games = games;
  }

  ngOnInit(): void {
    this.route.data.pipe(map(data => data.games)).subscribe((games: Game[]) => {
      this.initData(games);
    });
    this.headerService.setTitle('list.title');
    this.headerService.setIcon('menu');
    this.headerService.setIconClickHandler(() => {
      // show menu
    });
    this.headerService.setRefreshHandler(() => {
      this.refresh();
    });
    this.subscription = this.dataService.timeout
      .pipe(filter(data => data))
      .subscribe(() => {
        this.refresh();
    });
  }

  refresh() {
    this.dataService.getGames().subscribe((games: Game[]) => {
      this.initData(games);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
